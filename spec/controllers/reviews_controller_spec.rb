require 'spec_helper'

describe ReviewsController do
  let(:user) { create(:user) }
  let(:category) { create(:category) }
  let(:product) { create(:product, user: user, category: category) }
  let(:valid_attributes) do
    {
        content: 'lorem ipsum',
        rating: 5
    }
  end

  context 'user is signed in' do
    before(:each) do
      sign_in user
    end

    it "should assign current user as author" do
      post :create, {category_id: category.id, product_id: product.id, review: valid_attributes}
      expect(response).to redirect_to(category_product_url(category, product))
      expect(controller.review.user).to eq(controller.current_user)
    end
  end
end