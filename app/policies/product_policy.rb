class ProductPolicy
  attr_reader :user, :product

  def initialize(user, product)
    @product = product
    @user = user
  end

  def admin?
    @user.admin? ||
        @product.user == @user
  end
end