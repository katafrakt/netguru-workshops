class CategoryPolicy < ApplicationPolicy
  def admin?
    user.admin?
  end
end