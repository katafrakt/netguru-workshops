class Product < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  has_many :reviews

  validates :title, :price, :description, presence: true
  validates :price, format: {with: /\A\d+(?:\.\d{0,2})?\z/}, numericality: {minimum: 0}

  def average_rating
    self.reviews.average(:rating)
  end
end
